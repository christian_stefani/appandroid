package projectandroid.pse1718.group.com.digitspankotlin

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

//Classe che definisce l'activity di classifica, ovvero i giocatori del db con il relativo punteggio
class ScoreRanking : AppCompatActivity(){
    private val STRING_COUNT_RESTART = "count_restart"
    private val STRING_POSITION = "position"
    private val STRING_USER = "username"
    private val STRING_SCORE = "score"
    private val STRING_CONNESSION_YES = "yes_connession"

    private val mDatabase = FirebaseDatabase.getInstance().reference
    private val firebaseAuth = FirebaseAuth.getInstance()
    private var tv1 : TextView? = null
    private var tv2 : TextView? = null
    private var pos : TextView? = null      //posizione in classifica
    private var usr : TextView? = null      // utente
    private var scr : TextView? = null      //livello raggiunto
    private var totUser : String? = null
    private var totPos : String? = null
    private var totScr : String? = null
    private var countDb = 0
   // private var scorePos : IntArray = null
 //   private var userPos: Array<String>? = null


    private var countRest = 0
    private var yesCon = false



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(savedInstanceState!= null){
            onRestoreInstanceState(savedInstanceState)
        }
        setContentView(R.layout.activity_score_ranking)

        tv1 = findViewById<View>(R.id.usr) as TextView
        tv2 = findViewById<View>(R.id.scr) as TextView
        pos = findViewById<View>(R.id.rankingPos) as TextView
        usr = findViewById<View>(R.id.rankingUser) as TextView
        scr = findViewById<View>(R.id.rankingScore) as TextView
        val mEdge = findViewById<View>(R.id.edge) as TextView
        val score_offline = findViewById<View>(R.id.connession) as TextView

        countRest++

        /*Quando viene avviata l'activity:
        Se c'è la connessione visualizza la classifica
        altrimenti scrive che la classifica non è disponibile
         */
        if(countRest==1){
            if(checkConnession()){
                yesCon = true
                mEdge.visibility = View.VISIBLE
                queryScore()
            } else {
                yesCon = false
                val offline = "Classifica non disponibile"
                score_offline.text = offline
            }

        }

        /*Ripartendo l'activity:
        se al primo avvio c'era la connessione continua a visualizzare la classifica (non aggiornata)
        altrimenti continua a visualizzare che la classifica non è disponibile
         */
        if (countRest>1){
            if(yesCon){
                tv1!!.setText(R.string.username)
                tv2!!.setText(R.string.score)
                pos!!.text = totPos
                usr!!.text = totUser
                scr!!.text = totScr
                mEdge.visibility = View.VISIBLE
            } else {
                val offline = "Classifica non disponibile"
                score_offline.text = offline
            }
        }
    }

    private fun queryScore() {
        val query = mDatabase.child("users").orderByChild("score")
        val sb = StringBuilder()
        val sb1 = StringBuilder()
        val sb2 = StringBuilder()
        var scorePosit : String
        var userPosit : String
        var index : String

         //Query sul db prendendo tutti gli utenti e ordinandoli per score crescente (firebase non permette l'ordinamento decrescente)
        query.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                countDb = dataSnapshot.childrenCount.toInt()
                if (dataSnapshot.exists()) {
                    /*Contati il numero di utenti vengono fatte le operazioni:
                    Vengono creati 2 array di stringhe contenenti il primo i vari username ed il secondo i punteggi di ogni utente
                     */
                    val userPos = Array(countDb, {_ -> ""})
                    val scorePos = Array(countDb, {_ -> ""})
                    var y = countDb
                    for (users in dataSnapshot.children) {
                        val user1 = users!!.getValue(User::class.java)
                        if (user1 != null) {
                            var username = user1.username
                            if (username!!.length > 10) {
                                username = username.substring(0, 10) + "..."
                            }
                            val score : Int = user1.score
                            y--
                            userPos[y] = username
                            scorePos[y] = score.toString()

                        }
                    }
                    var k =1
                    //Vengono scritti i vari valori nei 3 StringBuilder (ognuno a capo rispetto l'altro)
                    for (i in scorePos.indices ) {
                        index = k.toString() + "\n"
                        userPosit = userPos[i] + "\n"
                        scorePosit = scorePos[i] + "\n"
                        sb.append(index)
                        sb1.append(userPosit)
                        sb2.append(scorePosit)
                        k++
                    }

                    //Vengono scritti i vari valori nelle rispettive TextView
                    totPos = sb.toString()
                    totUser = sb1.toString()
                    totScr = sb2.toString()

                    tv1!!.setText(R.string.username)
                    tv2!!.setText(R.string.score)
                    pos!!.text = totPos
                    usr!!.text = totUser
                    scr!!.text = totScr
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })
    }

    //Metodo che restituisce true se è presente le connessione dati, false altrimenti
    private fun checkConnession() : Boolean{
        val manager = applicationContext!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = manager.activeNetworkInfo
        return activeNetwork != null && (activeNetwork.type == ConnectivityManager.TYPE_WIFI || activeNetwork.type == ConnectivityManager.TYPE_MOBILE)
    }

     //Metodo che legge il valore del numero di tentativi dell'utente tramite dataSnapshot e incrementa di 1 tale valore
    private fun tentRestart(){

        mDatabase.child("users").child(firebaseAuth.currentUser!!.uid).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val user1 = dataSnapshot.getValue(User::class.java)
                var numTentNow = user1!!.numTent
                numTentNow++
                mDatabase.child("users").child(firebaseAuth.currentUser!!.uid).child("numTent").setValue(numTentNow)

            }
            override fun onCancelled(databaseError: DatabaseError) {

            }

            })

    }

    //Viene creato il menu in alto a destra per le 2 opzioni
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_ranking, menu)
        return true
    }

    //Azione da eseguire in base all'opzione cliccata
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val i = item!!.itemId
        when (i) {
            R.id.action_restart -> {
                tentRestart()
                Toast.makeText(applicationContext, "Il test riparte", Toast.LENGTH_SHORT).show()
                startActivity(Intent(this@ScoreRanking, GameActivity::class.java))
                finish()
                return true
            }
            R.id.action_update -> {
                if (checkConnession()) {
                    Toast.makeText(applicationContext, "Classifica aggiornata", Toast.LENGTH_SHORT).show()
                    startActivity(Intent(this@ScoreRanking, ScoreRanking::class.java))
                    finish()
                } else {
                    Toast.makeText(applicationContext, "Non è possibile aggiornare la classifica, nessuna connessione dati disponibile", Toast.LENGTH_LONG).show()
                }
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }

    }

   override fun onSaveInstanceState (savedInstanceState: Bundle?){
       super.onSaveInstanceState(savedInstanceState)
       savedInstanceState!!.putInt(STRING_COUNT_RESTART, countRest)
       savedInstanceState.putString(STRING_POSITION, totPos)
       savedInstanceState.putString(STRING_USER, totUser)
       savedInstanceState.putString(STRING_SCORE, totScr)
       savedInstanceState.putBoolean(STRING_CONNESSION_YES,yesCon)

    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        countRest = savedInstanceState!!.getInt(STRING_COUNT_RESTART)
        totPos = savedInstanceState.getString(STRING_POSITION)
        totUser = savedInstanceState.getString(STRING_USER)
        totScr = savedInstanceState.getString(STRING_SCORE)
        yesCon = savedInstanceState.getBoolean(STRING_CONNESSION_YES)
    }


    override fun onBackPressed() {
        startActivity(Intent(this@ScoreRanking, GameActivity::class.java))
        finish()
    }


}