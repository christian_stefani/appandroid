package projectandroid.pse1718.group.com.digitspankotlin

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.speech.tts.TextToSpeech

import java.util.Locale

//Classe che definisce il servizio del Text to Speech

class TTS : Service(), TextToSpeech.OnInitListener {
    var mTts: TextToSpeech? = null
    var seq = IntArray(GameActivity.count, { _-> 0})
    var countButton = 0


    override fun onCreate() {
        super.onCreate()
        mTts = TextToSpeech(this, this)
    }

    override fun onStartCommand(intent : Intent?, flags: Int, startId: Int): Int {
        mTts = TextToSpeech(this,this)
        seq = intent!!.getIntArrayExtra(GameActivity.STRING_INTENT_SEQ)
        countButton = intent.getIntExtra(GameActivity.STRING_INTENT_BUTTON,0 )
        return START_NOT_STICKY

    }

    override fun onInit(status : Int)
    {

        /*
        Settaggio dell'oggetto mTts tramite relativi metodi
        setLanguage: lingua utilizzata dal tts
        setPitch: intonazione, altezza voce
        setSpeechRate: velocità di pronuncia
         */
        if (status != TextToSpeech.ERROR) {
            mTts!!.language = Locale.ITALY
            mTts!!.setPitch(1f)
            mTts!!.setSpeechRate(1f)
        }
        if (countButton != 0) {
            for (i in 0 until GameActivity.count)
             {
                 val toSpeach = Integer.toString(seq[i])
                //metodo speak che legge la stringa che gli viene passata aggiungendo le nuove entry in fondo alla coda
                mTts!!.speak(toSpeach, TextToSpeech.QUEUE_ADD, null, null)
                //metodo per intervallare con una pausa da un numero all'altro di 1.3 secondi
                mTts!!.playSilentUtterance(1300, TextToSpeech.QUEUE_ADD, null)
            }
        }

    }

    override fun onDestroy()
    {
        //Stop del servizio
        mTts!!.stop()
        mTts!!.shutdown()
        super.onDestroy()
    }

    //Metodo per fare bind del service, che in questo caso non viene fatto
    override fun onBind(intent : Intent) : IBinder?
    {
        return null
    }

}