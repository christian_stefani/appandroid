package projectandroid.pse1718.group.com.digitspankotlin

//Classe che definisce l'utente del database
class User() {
    var username: String? = null
    var email: String? = null
    var numTent: Int = 0
    var score: Int = 0
    var in_game :Boolean = false

    //Costruttore con parametri (username, email, score, numeroTentativi, utente in gioco)
      constructor (username: String, email: String, score : Int, numTent: Int, in_game : Boolean ) : this(){
        this.username = username
        this.email = email
        this.score = score
        this.numTent = numTent
        this.in_game = in_game
    }

}