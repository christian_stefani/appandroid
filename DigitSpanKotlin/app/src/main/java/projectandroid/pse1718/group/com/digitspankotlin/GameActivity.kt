package projectandroid.pse1718.group.com.digitspankotlin


import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.media.AudioManager
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_game.*
import java.util.*

//Classe che definisce l'activity di gioco
class GameActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private val STRING_INSERT = "text"
    private val STRING_NUMBER = "text_num"
    private val STRING_RETRY = "retry_count"
    private val STRING_COUNT_BUTTON = "count_button"
    private val TAG = "GA" // tag debug GameActivity

    companion object {
        val STRING_INTENT_SEQ = "sequen"
        val STRING_INTENT_BUTTON = "countButton"
        var count = 5                      // numeri da cui e' composta la sequenza
    }

    private var sx: String? = null                       // sequenza da ascoltare
    private var numSeq = IntArray(count, {_->0})
    private var retry = 0
    private var countButton = 0
    private var serviceTTS: Intent? = null
    private var mBounded: Boolean = false
    private val mDatabase = SignInActivity.getDatabase().reference
    private val firebaseAuth = FirebaseAuth.getInstance()
    private var exitSign = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        this.volumeControlStream = AudioManager.STREAM_MUSIC
        setContentView(R.layout.activity_game)
        //Vengono visualizzate le istruzioni la prima volta che viene avviato il gioco
        if(isFirstRun()){
            alertDialogFirstTime()         //regole del gioco spiegate all'utente
        }

        val startB = findViewById<View>(R.id.startButton) as Button
        serviceTTS = Intent(applicationContext, TTS::class.java)

        //Viene calcolata una sequenza casuale di numeri interi ed inserita in una stringa
        numSeq = sequen(count)
        val sb = StringBuilder()
        for (i in 0 until count) {
            sb.append(numSeq[i])
        }
        sx = sb.toString()

        val counter = findViewById<View>(R.id.random_number_text_view) as TextView
        val textCounter = "sequenza "+ count+ " caratteri"
        counter.text = textCounter
        val mnum = findViewById<View>(R.id.sequence) as EditText
        val fm = fragmentManager
        val dialogFragment = MyDialogFragment()

        bindService(serviceTTS, mConnection, Context.BIND_AUTO_CREATE)

        /*Bottone per avviare l'ascolto della sequenza
        Viene richiamato il servizio del Text to speech
        viene sospesa l'activity e richiamata dopo un tempo calcolato in base alla lunghezza della sequenza
        il campo per poter inserire la sequenza viene nascosto durante questo intervallo, viene visualizzato alla fine
         */
        startB.setOnClickListener{
            countButton++
            if(countButton<3){
                window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                mnum.visibility = View.GONE
                mnum.text.clear()
                Log.d(TAG, "sequenza richiesta: "+ sx)
                dialogFragment.show(fm,"Dialog Fragment" )
                serviceTTS!!.putExtra(STRING_INTENT_SEQ, numSeq)
                serviceTTS!!.putExtra(STRING_INTENT_BUTTON,countButton)
                startService(serviceTTS)
                val r = Runnable {
                    window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                    dialogFragment.dismissAllowingStateLoss()
                    mnum.visibility = View.VISIBLE
                    }
                val handler = Handler()
                handler.postDelayed(r, (count*2200).toLong())

                }
            }
        //Metodo utilizzato per gestire cosa deve venir fatto nell'EditText in base al tasto cliccato della tastiera
        num_onClick(mnum)
        //Inizializzazione toolbar, DrawerLayout e NavigationView
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)


        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        val nav_view = findViewById<NavigationView>(R.id.nav_view)
        nav_view.setNavigationItemSelectedListener(this)
        val headerView = nav_view.getHeaderView(0)
        val navUsername = headerView!!.findViewById<View>(R.id.textViewEmail) as TextView
        navUsername.text = firebaseAuth!!.currentUser!!.email
    }

    //Gestione delle azioni quando viene cliccato il tasto back
    override fun onBackPressed() {
            System.gc()
            count = 5
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.addCategory(Intent.CATEGORY_HOME)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
    }


    /*Gestione delle 3 opzioni del NavigationDrawer
    -logout dell'utente
    -cancellazione account
    -visualizzazione classifica
     */
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        val id = item.itemId
        when (id) {
            R.id.action_logout -> {
                drawer.closeDrawer(GravityCompat.START)
                if (checkConnession()) {
                    alertDialogLogout()
                } else {
                    alertDialogConnession()
                }
                return true
            }
            R.id.action_delete -> {
                drawer.closeDrawer(GravityCompat.START)
                if (checkConnession()) {
                    alertDialogDelete()
                } else {
                    alertDialogConnession()
                }
                return true
            }
            R.id.action_score -> {
                drawer.closeDrawer(GravityCompat.START)
                alertDialogScore()
                return true
            }
        }

        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    //Metodo utilizzato per vedere se è la prima volta che viene avviata l'activity di gioco
    private fun isFirstRun(): Boolean{
        val runBefore = MySharedPreferences.getSharedPreferencesBoolean(applicationContext, MySharedPreferences.SharedPreferencesKey.runFirst, false)
        if(!runBefore){
            MySharedPreferences.putSharedPreferencesBoolean(applicationContext,MySharedPreferences.SharedPreferencesKey.runFirst, true)
        }
        return !runBefore
    }

    private fun randomInit(): Int{
        val r = Random()
        return r.nextInt(10)
    }

    private fun increment() :Int{
        count++
        return count
    }

    private fun sequen(count : Int) : IntArray{
        val seq = IntArray(count)
        for(i in seq.indices){
            seq[i] = randomInit()
        }
        return seq
    }

    override fun onSaveInstanceState(savedInstanceState: Bundle?) {
        super.onSaveInstanceState(savedInstanceState)
        savedInstanceState!!.putString(STRING_INSERT, sx)
        savedInstanceState.putIntArray(STRING_NUMBER, numSeq)
        savedInstanceState.putInt(STRING_RETRY, retry)
        savedInstanceState.putInt(STRING_COUNT_BUTTON, countButton)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        sx = savedInstanceState.getString(STRING_INSERT, "0")
        numSeq = savedInstanceState.getIntArray(STRING_NUMBER)
        retry = savedInstanceState.getInt(STRING_RETRY)
        countButton = savedInstanceState.getInt(STRING_COUNT_BUTTON)
    }

    //Viene mandato un intent per far ripartire l'activity
    private fun sendMessage() {
        val intent = Intent(this, GameActivity::class.java)
        startActivity(intent)
        finish()
    }

    //Metodo quando vengono commessi 2 errori per andare nell'activity dei punteggi (ScoreRanking)
    private fun sendRestart() {
        val intent = Intent(this, ScoreRanking::class.java)
        count = 5
        startActivity(intent)
        finish()
    }

    /*Metodo per gestire cosa deve venir fatto nell'EditText in base al tasto cliccato della tastiera
    Se la risposta è corretta scrive il punteggio nel db, lo incrementa di 1 e riparte l'activity
    Se è sbagliata cancella la sequenza appena digitata, al secondo errore termina il gioco e viene visualizzata la classifica
     */
    private fun num_onClick(mnum: EditText) {
        mnum.setOnEditorActionListener { v, KeyCode, event ->
            var handled = false
            if (KeyCode == EditorInfo.IME_ACTION_DONE || event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                val inputText = v.text.toString()
                if (inputText == sx) {
                    Toast.makeText(applicationContext, R.string.correct_question, Toast.LENGTH_SHORT).show()
                    val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    if ( currentFocus != null) {
                        inputManager.hideSoftInputFromWindow(currentFocus!!.windowToken,
                                InputMethodManager.HIDE_NOT_ALWAYS)
                    }
                    queryScore()
                    count = increment()
                    sendMessage()
                } else {
                    Toast.makeText(applicationContext, R.string.incorrect_question, Toast.LENGTH_SHORT).show()
                    val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    if (currentFocus != null) {
                        inputManager.hideSoftInputFromWindow(currentFocus!!.windowToken,
                                InputMethodManager.HIDE_NOT_ALWAYS)
                    }
                    v.text = ""
                    retry = setTry()
                    if (Retry()) {
                        sendRestart()
                    }
                }
                handled = true
            }
             handled
        }
    }

    /*Metodo utilizzato per leggere il valore nel db del punteggio, ed eventualmente aggiornarlo
    se quello registrato localmente è più alto di quello del db online lo aggiorna
    altrimenti non fa nulla
     */
    private fun queryScore(){
        val localScore = count
        val query = mDatabase.child("users").child(firebaseAuth.currentUser!!.uid).child("score")
        query.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    val score = dataSnapshot.getValue(Int::class.java)
                    if (localScore > score!!) {
                            mDatabase.child("users").child(firebaseAuth.currentUser!!.uid).child("score").setValue(localScore)
                    }

                }
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })
    }

    //Metodo che ritorna l'UID (Identificatore di ogni utente in firebase)
    private fun removeDataUser(): String{
        return firebaseAuth.currentUser!!.uid
    }

    //Metodo che cancella un utente e i relativi dati nel db
    private fun deleteAccount(){
        val uid = removeDataUser()
        firebaseAuth.currentUser!!.delete().addOnCompleteListener({ task ->
            if (task.isSuccessful) {
                mDatabase.child("users").child(uid).setValue(null)
                exitSign = true
                startActivity(Intent(this@GameActivity, SignInActivity::class.java))
                finish()
            } else {
                alertDialogDeleteUser()
            }
        })
    }

    /*Viene gestito il click del bottone back nel caso sia aperto il NavigationDrawer oppure no
    Se è aperto lo chiude
    Se non è aperto visualizza il messaggio contenuto nel alertDialogBack()
     */
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {
            Log.d("CDA", "onKeyDown Called")
            val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START)
            } else {
                alertDialogBack()
            }
            return true
        }
        return false
    }

    private fun Retry(): Boolean {
        return retry == 2
    }

    private fun setTry(): Int {
        retry++
        return retry
    }

    //AlertDialog per avvisare che non è possibile procedere con l'operazione in quanto non è presente nessuna connessione dati
    private fun alertDialogConnession(){
        val builder = AlertDialog.Builder(this, R.style.MyDialogTheme)
        builder.setTitle("CONNESSIONE DATI")
        builder.setMessage("Non è possibile procedere, non è presente nessuna connessione dati")

        val alert = builder.create()
        alert.show()
    }

    //AlertDialog che avvisa quando viene premuta l'opzione di logout nel NavigationDrawer
    private fun alertDialogLogout() {
            val builder = AlertDialog.Builder(this, R.style.MyDialogTheme)
            builder.setTitle("LOGOUT UTENTE")
            builder.setMessage("Se effettui il logout perderai l'attuale sessione di gioco, sei sicuro di voler proseguire?")
            builder.setCancelable(false)

            builder.setPositiveButton("SI") { dialog,  _->
                mDatabase.child("users").child(firebaseAuth.currentUser!!.uid).child("in_game").setValue(false)
                firebaseAuth!!.signOut()
                count = 5
                exitSign = true
                startActivity(Intent(this@GameActivity, SignInActivity::class.java))
                dialog.dismiss()
                finish()
            }

            builder.setNegativeButton("NO") { dialog, _ -> dialog.dismiss() }

            val alert = builder.create()
            alert.show()
    }

    //AlertDialog che avvisa quando viene premuta l'opzione di cancellazione account nel NavigationDrawer
    private fun alertDialogDelete() {
        val builder = AlertDialog.Builder(this, R.style.MyDialogTheme)
        builder.setTitle("CANCELLAZIONE UTENTE E DATI")
        builder.setMessage("Se prosegui cancellerai l'utente e i relativi dati, sei sicuro di voler proseguire?")
        builder.setCancelable(false)

        builder.setPositiveButton("SI") { dialog, _ ->
            count = 5
            deleteAccount()
            dialog.dismiss()
        }

        builder.setNegativeButton("NO") { dialog, _ -> dialog.dismiss() }

        val alert = builder.create()
        alert.show()
    }

    //AlertDialog che avvisa quando viene premuta l'opzione per visualizzare la classifica nel NavigationDrawer
    private fun alertDialogScore() {
        val builder = AlertDialog.Builder(this, R.style.MyDialogTheme)
        builder.setTitle("CLASSIFICA")
        builder.setMessage("Se prosegui per visualizzare la classifica perderai l'attuale sessione di gioco, sei sicuro di voler proseguire?")
        builder.setCancelable(false)

        builder.setPositiveButton("SI") { dialog, _ ->
            count = 5
            startActivity(Intent(this@GameActivity, ScoreRanking::class.java))
            finish()
            dialog.dismiss()
        }

        builder.setNegativeButton("NO") { dialog, _ -> dialog.dismiss() }

        val alert = builder.create()
        alert.show()
    }

    /*AlertDialog che avvisa l'utente che non può cancellare l'account in quanto è scaduto il token generato da firebase per l'utente
    Viene avvisato che se vuole cancellare l'account deve prima effettuare il logout
    poi riautenticarsi e procedere alla cancellazione
     */
    private fun alertDialogDeleteUser() {
        val builder = AlertDialog.Builder(this, R.style.MyDialogTheme)
        builder.setTitle("CANCELLAZIONE UTENTE E DATI")
        builder.setMessage("E' tracorso troppo tempo dall'ultima verifica dell'account, se vuoi continuare dovrai riautenticarti e solo dopo potrai procedere alla cancellazione, sei sicuro di voler continuare?")
        builder.setCancelable(false)

        builder.setPositiveButton("SI") { dialog, _ ->
            mDatabase.child("users").child(firebaseAuth.currentUser!!.uid).child("in_game").setValue(false)
            firebaseAuth.signOut()
            startActivity(Intent(this@GameActivity, SignInActivity::class.java))
            exitSign = true
            finish()
            dialog.dismiss()
        }

        builder.setNegativeButton("NO") { dialog, _ -> dialog.dismiss() }

        val alert = builder.create()
        alert.show()
    }

    //AlertDialog quando viene premuto il tasto back
    private fun alertDialogBack() {
        val builder = AlertDialog.Builder(this, R.style.MyDialogTheme)
        builder.setTitle("ATTENZIONE!!!")
        builder.setMessage("Hai premouto il tasto back, se proseguirai perderai l'attuale sessione di gioco, sei sicuro di voler proseguire?")
        builder.setCancelable(false)

        builder.setPositiveButton("SI") { dialog, _ ->
            if (checkConnession()) {
                mDatabase.child("users").child(firebaseAuth!!.currentUser!!.uid).child("in_game").setValue(false).addOnCompleteListener({ task ->
                    if (task.isSuccessful) {
                        onBackPressed()
                        dialog.dismiss()
                    } else {
                        Toast.makeText(this@GameActivity, "Errore", Toast.LENGTH_SHORT).show()
                        dialog.dismiss()
                    }
                })
            } else {
                onBackPressed()
                dialog.dismiss()
            }
        }

        builder.setNegativeButton("NO") { dialog, _ -> dialog.dismiss() }

        val alert = builder.create()
        alert.show()
    }

    //AlertDialog che viene visualizzato solamente la prima volta che viene avviato il gioco
    private fun alertDialogFirstTime() {
        val builder = AlertDialog.Builder(this, R.style.MyDialogTheme)
        builder.setTitle("REGOLE")
        builder.setMessage("Ascolta la sequenza di numeri e scrivila nell'apposito campo. Se rispondi correttamente verrà aggiunto un numero alla sequenza successiva. Hai 2 tentativi per ogni livello")
        builder.setCancelable(false)

        builder.setPositiveButton("Ok") { dialog, _ -> dialog.dismiss() }

        val alert = builder.create()
        alert.show()
    }

    //Metodo che restituisce true se è presente le connessione dati, false altrimenti
    private fun checkConnession(): Boolean {
        val manager = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = manager.activeNetworkInfo
        return activeNetwork != null && (activeNetwork.type == ConnectivityManager.TYPE_WIFI || activeNetwork.type == ConnectivityManager.TYPE_MOBILE)

    }


    //Oggetto di tipo ServiceConnection che gestisce la connessione e disconnessione con il service
    private val mConnection = object : ServiceConnection {

        override fun onServiceDisconnected(name: ComponentName) {
            mBounded = false
        }

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            mBounded = true
        }
    }


    public override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
        //Viene fatto il bind del service
        if(!mBounded){
            bindService(serviceTTS, mConnection, Context.BIND_AUTO_CREATE)
            mBounded = true
        }
        //Viene settato nel database il valore true del campo in_game (ovvero l'utente è in gioco)
        mDatabase.child("users").child(firebaseAuth.currentUser!!.uid).child("in_game").setValue(true)

    }

    override fun onPause() {
        super.onPause()
        //Viene fatto l'unbind del service
        if(mBounded) {
            unbindService(mConnection)
            mBounded = false
        }
        //Viene settato nel database il valore false del campo in_game (ovvero l'utente non è attualmente in gioco)
        if(!exitSign) {
            mDatabase.child("users").child(firebaseAuth.currentUser!!.uid).child("in_game").setValue(false)
        }
    }

    override fun onStop(){
        super.onStop()
        if(mBounded){
            unbindService(mConnection)
            mBounded= false
        }
    }



}
