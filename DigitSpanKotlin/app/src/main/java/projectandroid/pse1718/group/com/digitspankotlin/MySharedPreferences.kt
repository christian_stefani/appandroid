package projectandroid.pse1718.group.com.digitspankotlin

import android.content.Context
import android.preference.PreferenceManager
import android.content.SharedPreferences


//Classe statica che permette di gestire le SharedPreferences tra le varie activity
class MySharedPreferences{

    //viene utilizzato companion object per rendere le costanti e i metodi accessibili da altre classi

    class SharedPreferencesKey {
        companion object {
            val runFirst = "run_first_time"
            val runFirstSign = "run_first_sign"
        }
    }
    companion object {
        //Aggiunge una SharedPreference di tipo boolean
        fun putSharedPreferencesBoolean(context: Context, key: String, v : Boolean){
            val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
            val edit : SharedPreferences.Editor = preferences.edit()
            edit.putBoolean(key, v)
            edit.apply()
        }
        //Ritorna il valore di una SharedPreference di tipo boolean
        fun getSharedPreferencesBoolean(context: Context, key: String, _default: Boolean): Boolean{
            val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
            return preferences.getBoolean(key, _default)
        }
    }

}