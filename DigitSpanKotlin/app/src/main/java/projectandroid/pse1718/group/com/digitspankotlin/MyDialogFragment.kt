package projectandroid.pse1718.group.com.digitspankotlin

import android.app.DialogFragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

//Classe che visualizza il DialogFragment nella GameActivity (quando si è in ascolto della sequenza)
class MyDialogFragment : DialogFragment(){



    override fun onDestroyView() {
        val dialog = dialog
        if(dialog != null && retainInstance){
            dialog.setDismissMessage(null)
        }
        super.onDestroyView()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    //Viene creata la rispettiva View tramite il file di layout specificato, settandola non cancellabile
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val MainView = inflater!!.inflate(R.layout.fragment_dialog, container, false)
        dialog.setTitle("Text to Speech")
        isCancelable = false
        return MainView
    }

}