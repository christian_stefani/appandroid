package projectandroid.pse1718.group.com.digitspankotlin

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

//Classe che definisce l'activity di reset della password di un utente
 class ResetPwd : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_pwd)

        val edtEmail = findViewById<View>(R.id.edt_reset_email) as EditText
        val btnResetPassword = findViewById<View>(R.id.btn_reset_password) as Button
        val btnBack = findViewById<View>(R.id.btn_back) as Button

        val mAuth = FirebaseAuth.getInstance()

        /*Quando viene cliccato il bottone Reset Password
        Se è presente la connessione dati viene inviata una mail per poter resettare la password all'utente specificato
        Altrimenti visualizza il messaggio che non è presente la connessione
         */
        btnResetPassword.setOnClickListener{
            if (checkConnession()){
                val email = edtEmail.text.toString().trim()
                if(TextUtils.isEmpty(email)){
                    Toast.makeText(applicationContext, "Inserisci la tua email!", Toast.LENGTH_SHORT).show()
                }
                mAuth.sendPasswordResetEmail(email).addOnCompleteListener{ task ->
                    if(task.isSuccessful){
                        Toast.makeText(applicationContext, "Controlla la mail per resettare la password!", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(applicationContext, "Errore nell'invio della mail!", Toast.LENGTH_SHORT).show()
                    }
                }
            } else{
                alertDialogConnession()
            }

        }
        btnBack.setOnClickListener{
            startActivity(Intent(this, SignInActivity::class.java ))
            finish()
        }


    }

    //Metodo che restituisce true se è presente le connessione dati, false altrimenti
    private fun checkConnession(): Boolean{
        val manager = applicationContext!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = manager.activeNetworkInfo
        return activeNetwork != null && (activeNetwork.type == ConnectivityManager.TYPE_WIFI || activeNetwork.type == ConnectivityManager.TYPE_MOBILE)

    }

    //AlertDialog che viene visualizzato nel caso non sia presente la connessione
    private fun alertDialogConnession(){
        val builder = AlertDialog.Builder(this, R.style.MyDialogTheme)
        builder.setTitle("CONNESSIONE DATI")
        builder.setMessage("Nessuna connessione dati, per poter resettare la password utilizza una connessione dati mobile oppure WI-FI")

        val alert = builder.create()
        alert.show()
    }

    override fun onBackPressed(){
        startActivity(Intent(this,SignInActivity::class.java ))
        finish()
    }



}