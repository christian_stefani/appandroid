package projectandroid.pse1718.group.com.digitspankotlin

import android.Manifest
import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.*
import com.google.firebase.database.*


//Classe che definisce l'activity per la creazione di un account e il rispettivo login
class SignInActivity : AppCompatActivity(){
    private val TAG = "try_catch"
    var mDatabase : DatabaseReference? = null
    var mAuth : FirebaseAuth? = null
    var mEmailField : EditText? = null
    var mPasswordField: EditText? = null
    // elementi che devono essere statici per essere condivisi con altre classi del progetto
    companion object {
        private val STORAGE_PERMISSION_CODE = 23
        var mDb : FirebaseDatabase? = null
        //Metodo che ritorna un istanza del database settandolo in maniera persistente, ovvero salvato nella memoria del dispositivo
        fun getDatabase(): FirebaseDatabase {
            if (mDb == null) {
                mDb = FirebaseDatabase.getInstance()
                mDb!!.setPersistenceEnabled(true)
            }
            return mDb as FirebaseDatabase
        }
    }

    override fun onCreate(savedInstanceState : Bundle?){
        super.onCreate(savedInstanceState)

        supportActionBar!!.setTitle(R.string.title_activity_login)

        //Al primo avvio dell'app vengono richiesti i permessi di lettura e scrittura nella memoria del telefono
        if (isFirstRun()){
            allertDialogPerm()
        } else {
            if (Build.VERSION.SDK_INT>=23){
                ActivityCompat.requestPermissions(this@SignInActivity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE), STORAGE_PERMISSION_CODE)
            }
        }
        setContentView(R.layout.activity_sign_in)
        mDatabase = getDatabase().reference
        mDatabase!!.keepSynced(true)
        mAuth = FirebaseAuth.getInstance()
        mEmailField = findViewById(R.id.field_email)
        mPasswordField = findViewById(R.id.field_password)
        val mSignInButton = findViewById<View>(R.id.button_sign_in) as Button
        val mNewAccountButton = findViewById<View>(R.id.button_new_account) as Button
        val resetPwd = findViewById<View>(R.id.button_reset_pwd) as Button

        mSignInButton.setOnClickListener({
            if (checkConnession()) {
                signIn()
            } else {
                alertDialogConnession()
            }
        })

        mNewAccountButton.setOnClickListener( {
            if (checkConnession()) {
                newAccount()
            } else {
                alertDialogConnession()
            }
        })

        resetPwd.setOnClickListener( {
            startActivity(Intent(this@SignInActivity, ResetPwd::class.java))
                finish()
        })

    }


    override fun onStart(){
        super.onStart()
        //Se viene autenticato l'utente e l'email è già stata verificata allora parte il gioco
        if(mAuth!!.currentUser != null) {
            if (mAuth!!.currentUser!!.isEmailVerified) {
                onAuthSuccess()
            }
        }
    }

    @TargetApi(23)
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            STORAGE_PERMISSION_CODE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                finish()
                System.exit(0)
            }
        }
    }

    //Metodo che restituisce true se è presente le connessione dati, false altrimenti
    private fun checkConnession() : Boolean{
        val manager = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = manager.activeNetworkInfo
        return activeNetwork != null && (activeNetwork.type == ConnectivityManager.TYPE_WIFI || activeNetwork.type == ConnectivityManager.TYPE_MOBILE)

    }

    //Metodo per verificare salvando nelle SharedPreferences se è il primo avvio dell'applicazione
    private fun isFirstRun() : Boolean{
        val runBefore = MySharedPreferences.getSharedPreferencesBoolean(applicationContext, MySharedPreferences.SharedPreferencesKey.runFirstSign, false)
        if(!runBefore){
            MySharedPreferences.putSharedPreferencesBoolean(applicationContext,MySharedPreferences.SharedPreferencesKey.runFirstSign, true)
        }
        return !runBefore
    }

    private fun newAccount(){
        if (notValidateForm()){
            return
        }
        val email : String = mEmailField!!.text.toString()
        val password : String = mPasswordField!!.text.toString()

        /*Creazione di un nuovo utente tramite email e password
        Se l'operazione avviene con successo viene invitata una mail per verificare l'account
        Viene salvato nelle SharedPreferences il valore runFirs false, in modo che poi al primo login successivo vengano visualizzate le istruzioni del gioco
        Vengono gestite le eccezioni di password troppo corta, credenziali errate e se è già presente un utente con la stessa mail
         */
        mAuth!!.createUserWithEmailAndPassword(email,password).addOnCompleteListener(this) { task ->
            if (task.isSuccessful) {
                val user = task.result.user
                if(user.email != null){
                    val username : String = usernameFromEmail(user.email!!)
                    writeNewUser(user.uid, username, user.email!!)
                    sendEmailVerification()
                    MySharedPreferences.putSharedPreferencesBoolean(applicationContext, MySharedPreferences.SharedPreferencesKey.runFirst, false)

                }
            } else{
                try {
                    throw (task.exception!!)
                } catch (e: FirebaseAuthWeakPasswordException) {
                    Toast.makeText(applicationContext, "Password troppo corta (minimo 6 caratteri)", Toast.LENGTH_LONG).show()
                } catch (e: FirebaseAuthInvalidCredentialsException) {
                    Toast.makeText(applicationContext, "Credenziali errate", Toast.LENGTH_LONG).show()
                } catch (e: FirebaseAuthUserCollisionException) {
                    Toast.makeText(applicationContext, "Collisione con altro utente già registrato", Toast.LENGTH_LONG).show()
                } catch (e: Exception) {
                    Log.e(TAG, e.message)
                }

            }

        }
    }

    private fun signIn(){
        if (notValidateForm()){
            return
        }
        val email : String = mEmailField!!.text!!.toString()
        val password : String = mPasswordField!!.text!!.toString()

        /*Autenticazione utente tramite email e password
        Se la mail dell'utente è già stata verificata setta i valori nel db, altrimenti riporta che l'utente non è ancora stato verificato
        Vengono gestite le eccezioni nel caso l'email inserita non esista nel db oppure la password inserita non sia corretta
         */
        mAuth!!.signInWithEmailAndPassword(email, password).addOnCompleteListener(this){task ->
            if (task.isSuccessful){
                if (mAuth!!.currentUser!!.isEmailVerified){
                    val user = task.result.user
                    mDatabase!!.child("users").child(user.uid).child("in_game").setValue(true)
                    onAuthSuccess()

                }
                if(!mAuth!!.currentUser!!.isEmailVerified){
                    Toast.makeText(applicationContext, "Utente non ancora verificato", Toast.LENGTH_LONG).show()
                }
            } else {
                    try {
                        throw (task.exception!!)
                    } catch (e: FirebaseAuthInvalidUserException) {
                        Toast.makeText(applicationContext, "L'email inserita non esiste", Toast.LENGTH_LONG).show()
                    } catch (e: FirebaseAuthInvalidCredentialsException) {
                        Toast.makeText(applicationContext, "La password inserita non è corretta", Toast.LENGTH_LONG).show()
                    } catch (e: Exception) {
                        Log.e(TAG, e.message)
                    }

            }
        }
    }

    /*Metodo utilizzato nel caso l'autenticazione vada a buon fine
    viene incrementato il valore del numero di tentativi di gioco di 1 (tentRestart())
    viene eseguita l'activity di gioco
     */

    private fun onAuthSuccess(){
        tentRestart()
        startActivity(Intent(this@SignInActivity, GameActivity::class.java))

    }

    //Metodo che restituisce l'username dall'email
    private fun usernameFromEmail(email: String): String{
        return if (email.contains("@")){
            email.split("@")[0]
        } else {
            email
        }
    }

    //Metodo per verificare se vengono lasciati vuoti i campi email e password (necessari)
    private fun notValidateForm(): Boolean{
        var result = false
        if (TextUtils.isEmpty(mEmailField!!.text.toString())){
            mEmailField!!.error = "Required"
            result = true
        } else {
            mEmailField!!.error = null
        }
        if (TextUtils.isEmpty(mPasswordField!!.text.toString())){
            mPasswordField!!.error = "Required"
            result = true
        } else {
            mPasswordField!!.error = null
        }
        return result
    }

    //Metodo utilizzato per mandare un'email di verifica all'utente alla creazione dell'account
    private fun sendEmailVerification() {
        val user = mAuth!!.currentUser
        user!!.sendEmailVerification()
                .addOnCompleteListener(this,  { task ->
                    if (task.isSuccessful) {
                        Toast.makeText(this@SignInActivity, "Email di verifica spedita a: " + user.email, Toast.LENGTH_LONG).show()
                    } else {
                        Log.e(TAG, "sendEmailVerification", task.exception)
                        Toast.makeText(this@SignInActivity, "Errore nell'invio della mail di verifica", Toast.LENGTH_LONG).show()
                    }
                })

    }

    //Crea un nuovo utente e lo inseriesce nel db
    private fun writeNewUser(userId : String, name : String, email : String){
        val user = User(name, email,0,0,false)
        mDatabase!!.child("users").child(userId).setValue(user)
    }

    //AlertDialog che avvisa che non c'è la connessione dati
    private fun alertDialogConnession() {
        val builder = AlertDialog.Builder(this, R.style.MyDialogTheme)
        builder.setTitle("CONNESSIONE DATI")
        builder.setMessage("Nessuna connessione dati, per poter creare un account o fare il login utilizza una connessione dati mobile oppure WI-FI")
        val alert = builder.create()
        alert.show()
    }

    //AlertDialog che avvisa al primo avvio dell'app che servono i permessi
    private fun allertDialogPerm(){
        val builder = AlertDialog.Builder(this, R.style.MyDialogTheme)
        builder.setTitle("PERMESSI RICHIESTI!!!")
        builder.setMessage("L'app richiede i permessi di lettura e scrittura in memoria."+"\n"+"Negando tale richiesta l'app non funzionerà!")
        builder.setCancelable(false)

        builder.setPositiveButton("OK") { dialog, _ ->
            if (Build.VERSION.SDK_INT >= 23) {
                ActivityCompat.requestPermissions(this@SignInActivity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE), STORAGE_PERMISSION_CODE)
            }
            dialog.dismiss()
        }

        val alert = builder.create()
        alert.show()
    }

    //Metodo che legge il valore del numero di tentativi dell'utente tramite dataSnapshot e incrementa di 1 tale valore
    private fun tentRestart(){
        mDatabase!!.child("users").child(mAuth!!.currentUser!!.uid).addListenerForSingleValueEvent(
                object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        val user1 = dataSnapshot.getValue<User>(User::class.java)
                        var numTentNow = user1!!.numTent
                        numTentNow++
                        mDatabase!!.child("users").child(mAuth!!.currentUser!!.uid).child("numTent").setValue(numTentNow)
                     }
                    override fun onCancelled(databaseError: DatabaseError) {
                        }
                })

    }


    //Override del metodo onBackPressed() per gestire cosa deve venire fatto premendo il tasto back
    override fun onBackPressed(){
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.addCategory(Intent.CATEGORY_HOME)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)

    }


}