package projectandroid.pse1718.group.com.digitspanjava;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

//Classe che definisce l'activity di classifica, ovvero i giocatori del db con il relativo punteggio
public class ScoreRanking extends AppCompatActivity {

    private static final String STRING_COUNT_RESTART = "count_restart";
    private static final String STRING_POSITION = "position";
    private static final String STRING_USER= "username";
    private static final String STRING_SCORE = "score";
    private static final String STRING_CONNESSION_YES = "yes_connession";


    private final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
    private final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    private TextView tv1;
    private TextView tv2;
    private TextView pos;   //posizione in classifica
    private TextView usr;   // utente
    private TextView scr;   //livello raggiunto
    private String totUser;
    private String totPos;
    private String totScr;
    private int countDb = 0;
    private int scorePos[];
    private String userPos[];
    private String scorePosit;
    private String userPosit;
    private String index;
    private int countRest = 0;
    private boolean yesCon = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            onRestoreInstanceState(savedInstanceState);
        }

        setContentView(R.layout.activity_score_ranking);

        tv1 = findViewById(R.id.usr);
        tv2 = findViewById(R.id.scr);
        pos = findViewById(R.id.rankingPos);
        usr = findViewById(R.id.rankingUser);
        scr = findViewById(R.id.rankingScore);
        final TextView mEdge = findViewById(R.id.edge);
        final TextView score_offline = findViewById(R.id.connession);

        countRest++;

        /*Quando viene avviata l'activity:
        Se c'è la connessione visualizza la classifica
        altrimenti scrive che la classifica non è disponibile
         */
        if(countRest==1) {
            if (checkConnession()) {
                yesCon = true;
                mEdge.setVisibility(View.VISIBLE);
                queryScore();
            } else {
                yesCon = false;
                String offline = "Classifica non disponibile";
                score_offline.setText(offline);
            }
        }

        /*Ripartendo l'activity:
        se al primo avvio c'era la connessione continua a visualizzare la classifica (non aggiornata)
        altrimenti continua a visualizzare che la classifica non è disponibile
         */
        if(countRest>1) {
            if (yesCon) {
                tv1.setText(R.string.username);
                tv2.setText(R.string.score);
                pos.setText(totPos);
                usr.setText(totUser);
                scr.setText(totScr);
                mEdge.setVisibility(View.VISIBLE);
            }
            else {
                String offline = "Classifica non disponibile";
                score_offline.setText(offline);
            }
        }


    }

    private void queryScore() {
        Query query = mDatabase.child("users").orderByChild("score");
        final StringBuilder sb = new StringBuilder();
        final StringBuilder sb1 = new StringBuilder();
        final StringBuilder sb2 = new StringBuilder();

        //Query sul db prendendo tutti gli utenti e ordinandoli per score crescente (firebase non permette l'ordinamento decrescente)
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                countDb = (int) dataSnapshot.getChildrenCount();
                if (dataSnapshot.exists()) {
                    /*Contati il numero di utenti vengono fatte le operazioni:
                    Vengono creati 2 array di stringhe contenenti il primo i vari username ed il secondo i punteggi di ogni utente
                     */
                    userPos = new String[countDb];
                    scorePos = new int[countDb];
                    int y=0;
                    for (DataSnapshot users : dataSnapshot.getChildren()) {
                        User user1 = users.getValue(User.class);
                        if (user1 != null) {
                            String username = user1.getUsername();
                            if (username.length() > 10) {
                                username = username.substring(0,10)+ "...";
                            }
                            int score = user1.getScore();
                            userPos[y] = username;
                            scorePos[y] = score;
                            y++;
                        }
                    }
                    //Vengono scritti i vari valori nei 3 StringBuilder (ognuno a capo rispetto l'altro)
                        for (int i = countDb - 1,  k =1;  i >= 0; i--, k++) {
                            index = k + "\n";
                            userPosit = userPos[i]+"\n";
                            scorePosit = scorePos[i]+"\n";
                            sb.append(index);
                            sb1.append(userPosit);
                            sb2.append(scorePosit);
                        }

                    //Vengono scritti i vari valori nelle rispettive TextView
                    totPos = sb.toString();
                    totUser = sb1.toString();
                    totScr = sb2.toString();

                    tv1.setText(R.string.username);
                    tv2.setText(R.string.score);
                    pos.setText(totPos);
                    usr.setText(totUser);
                    scr.setText(totScr);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        }

    //Metodo che restituisce true se è presente le connessione dati, false altrimenti
    private boolean checkConnession() {
        ConnectivityManager manager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (manager != null) {
            NetworkInfo activeNetwork = manager.getActiveNetworkInfo();
            return activeNetwork != null && (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE);
        }
        return false;
    }

    //Metodo che legge il valore del numero di tentativi dell'utente tramite dataSnapshot e incrementa di 1 tale valore
    private void tentRestart() {
        if (firebaseAuth.getCurrentUser() != null) {
            mDatabase.child("users").child(firebaseAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(
                    new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            User user1 = dataSnapshot.getValue(User.class);
                            if (user1 != null) {
                                int numTentNow = user1.getNumTent();
                                numTentNow++;
                                mDatabase.child("users").child(firebaseAuth.getCurrentUser().getUid()).child("numTent").setValue(numTentNow);
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
        }
    }

    //Viene creato il menu in alto a destra per le 2 opzioni
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_ranking, menu);
        return true;
    }

    //Azione da eseguire in base all'opzione cliccata
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        switch (i) {
            case R.id.action_restart:
                tentRestart();
                Toast.makeText(getApplicationContext(), "Il test riparte", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(ScoreRanking.this, GameActivity.class));
                finish();
                return true;
            case R.id.action_update:
                    if(checkConnession()) {
                        Toast.makeText(getApplicationContext(), "Classifica aggiornata", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(ScoreRanking.this, ScoreRanking.class));
                        finish();
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Non è possibile aggiornare la classifica, nessuna connessione dati disponibile", Toast.LENGTH_LONG).show();
                    }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt(STRING_COUNT_RESTART,countRest);
        savedInstanceState.putString(STRING_POSITION,totPos);
        savedInstanceState.putString(STRING_USER,totUser);
        savedInstanceState.putString(STRING_SCORE,totScr);
        savedInstanceState.putBoolean(STRING_CONNESSION_YES,yesCon);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        countRest = savedInstanceState.getInt(STRING_COUNT_RESTART);
        totPos = savedInstanceState.getString(STRING_POSITION);
        totUser = savedInstanceState.getString(STRING_USER);
        totScr = savedInstanceState.getString(STRING_SCORE);
        yesCon = savedInstanceState.getBoolean(STRING_CONNESSION_YES);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    @Override
    public void onBackPressed(){
        startActivity(new Intent(ScoreRanking.this, GameActivity.class));
        finish();
    }

}
