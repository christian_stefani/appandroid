package projectandroid.pse1718.group.com.digitspanjava;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

//Classe statica che permette di gestire le SharedPreferences tra le varie activity
class MySharedPreferences {
    public static class SharedPreferencesKeys{
        public static final String runFirst="run_first_time";
        public static final String runFirstSign = "run_first_sign";
    }

    //Aggiunge una SharedPreference di tipo boolean
    public static void putSharedPreferencesBoolean(Context context, String key, boolean val){
        SharedPreferences preferences=PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit=preferences.edit();
        edit.putBoolean(key, val);
        edit.apply();
    }

    //Ritorna il valore di una SharedPreference di tipo boolean
    public static boolean getSharedPreferencesBoolean(Context context, String key, boolean _default){
        SharedPreferences preferences=PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(key, _default);
    }
}

