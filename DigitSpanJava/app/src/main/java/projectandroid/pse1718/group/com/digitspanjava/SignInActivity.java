package projectandroid.pse1718.group.com.digitspanjava;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

//Classe che definisce l'activity per la creazione di un account e il rispettivo login
public class SignInActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "try_catch";

    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private EditText mEmailField;
    private EditText mPasswordField;
    private static FirebaseDatabase mDb;
    private static final int STORAGE_PERMISSION_CODE=23;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.title_activity_login);

        //Al primo avvio dell'app vengono richiesti i permessi di lettura e scrittura nella memoria del telefono
        if(isFirstRun()) {
            alertDialogPerm();
        }
        else {
            if (Build.VERSION.SDK_INT>=23) {
                ActivityCompat.requestPermissions(SignInActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
            }
        }

        setContentView(R.layout.activity_sign_in);
        mDatabase = getDatabase().getReference();
        mDatabase.keepSynced(true);
        mAuth = FirebaseAuth.getInstance();
        mEmailField = findViewById(R.id.field_email);
        mPasswordField = findViewById(R.id.field_password);
        Button mSignInButton = findViewById(R.id.button_sign_in);
        Button mNewAccountButton = findViewById(R.id.button_new_account);
        Button resetPwd = findViewById(R.id.button_reset_pwd);
        mSignInButton.setOnClickListener(this);
        mNewAccountButton.setOnClickListener(this);
        resetPwd.setOnClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        //Se viene autenticato l'utente e l'email è già stata verificata allora parte il gioco
        if (mAuth.getCurrentUser() != null) {
            if (mAuth.getCurrentUser().isEmailVerified()) {
                onAuthSuccess();
            }
        }
    }

    //Metodo per verificare salvando nelle SharedPreferences se è il primo avvio dell'applicazione
    private boolean isFirstRun() {
        boolean runBefore = MySharedPreferences.getSharedPreferencesBoolean(getApplicationContext(), MySharedPreferences.SharedPreferencesKeys.runFirstSign, false);
        if (!runBefore) {
            MySharedPreferences.putSharedPreferencesBoolean(getApplicationContext(),MySharedPreferences.SharedPreferencesKeys.runFirstSign, true);
        }
        return !runBefore;
    }

    @TargetApi(23)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case STORAGE_PERMISSION_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    finish();
                    System.exit(0);
                }
                break;
        }
    }

    //Metodo statico che ritorna un istanza del database settandolo in maniera persistente, ovvero salvato nella memoria del dispositivo
    public static FirebaseDatabase getDatabase() {
        if (mDb == null) {
            mDb = FirebaseDatabase.getInstance();
            mDb.setPersistenceEnabled(true);
        }
        return mDb;
    }

    //Metodo che restituisce true se è presente le connessione dati, false altrimenti
    private boolean checkConnession() {
        ConnectivityManager manager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (manager != null) {
            NetworkInfo activeNetwork = manager.getActiveNetworkInfo();
            return activeNetwork != null && (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE);
        }
    return false;
}

    private void signIn(){
        if (notValidateForm()){
            return;
        }

        final String email = mEmailField.getText().toString();
        String password = mPasswordField.getText().toString();

        /*Autenticazione utente tramite email e password
        Se la mail dell'utente è già stata verificata setta i valori nel db, altrimenti riporta che l'utente non è ancora stato verificato
        Vengono gestite le eccezioni nel caso l'email inserita non esista nel db oppure la password inserita non sia corretta
         */
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {
                            if (mAuth.getCurrentUser() != null) {
                                if (mAuth.getCurrentUser().isEmailVerified()) {
                                    final FirebaseUser user = task.getResult().getUser();
                                    mDatabase.child("users").child(user.getUid()).child("in_game").setValue(true);
                                    onAuthSuccess();
                                }
                            }
                            if(!mAuth.getCurrentUser().isEmailVerified()) {
                                Toast.makeText(getApplicationContext(), "Utente non ancora verificato", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            try {
                                throw Objects.requireNonNull(task.getException());
                            } catch(FirebaseAuthInvalidUserException e) {
                                Toast.makeText(getApplicationContext(), "L'email inserita non esiste", Toast.LENGTH_LONG).show();
                            } catch(FirebaseAuthInvalidCredentialsException e) {
                                Toast.makeText(getApplicationContext(), "La password inserita non è corretta", Toast.LENGTH_LONG).show();
                            }
                            catch(Exception e) {
                                Log.e(TAG, e.getMessage());
                            }
                        }
                    }
                });

    }

    private void newAccount(){

        if (notValidateForm()){
            return;
        }
        String email = mEmailField.getText().toString();
        String password = mPasswordField.getText().toString();

        /*Creazione di un nuovo utente tramite email e password
        Se l'operazione avviene con successo viene invitata una mail per verificare l'account
        Viene salvato nelle SharedPreferences il valore runFirs false, in modo che poi al primo login successivo vengano visualizzate le istruzioni del gioco
        Vengono gestite le eccezioni di password troppo corta, credenziali errate e se è già presente un utente con la stessa mail
         */
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>(){
                    @Override
                    public void onComplete (@NonNull Task<AuthResult> task){
                        if (task.isSuccessful()){
                            FirebaseUser user = task.getResult().getUser();
                            if(user.getEmail() != null) {
                                String username = usernameFromEmail(user.getEmail());
                                writeNewUser(user.getUid(), username, user.getEmail());
                                sendEmailVerification();
                                MySharedPreferences.putSharedPreferencesBoolean(getApplicationContext(),MySharedPreferences.SharedPreferencesKeys.runFirst, false);
                            }
                        } else {
                            try {
                                throw Objects.requireNonNull(task.getException());
                            } catch(FirebaseAuthWeakPasswordException e) {
                                Toast.makeText(getApplicationContext(), "Password troppo corta (minimo 6 caratteri)", Toast.LENGTH_LONG).show();
                            } catch(FirebaseAuthInvalidCredentialsException e) {
                                Toast.makeText(getApplicationContext(), "Credenziali errate", Toast.LENGTH_LONG).show();
                            } catch(FirebaseAuthUserCollisionException e) {
                                Toast.makeText(getApplicationContext(), "Collisione con altro utente già registrato", Toast.LENGTH_LONG).show();
                            } catch(Exception e) {
                                Log.e(TAG, e.getMessage());
                            }
                        }
                    }
                });
    }

    /*Metodo utilizzato nel caso l'autenticazione vada a buon fine
    viene incrementato il valore del numero di tentativi di gioco di 1 (tentRestart())
    viene eseguita l'activity di gioco
     */
    private void onAuthSuccess() {
        tentRestart();
        startActivity(new Intent(SignInActivity.this, GameActivity.class));
        finish();
    }

    //Metodo che restituisce l'username dall'email
    private String usernameFromEmail(String email) {
        if (email.contains("@")) {
            return email.split("@")[0];
        } else {
            return email;
        }
    }

    //Metodo per verificare se vengono lasciati vuoti i campi email e password (necessari)
    private boolean notValidateForm() {
        boolean result = false;
        if (TextUtils.isEmpty(mEmailField.getText().toString())) {
            mEmailField.setError("Required");
            result = true;
        } else {
            mEmailField.setError(null);
        }
        if (TextUtils.isEmpty(mPasswordField.getText().toString())) {
            mPasswordField.setError("Required");
            result = true;
        } else {
            mPasswordField.setError(null);
        }

        return result;
    }

    //Metodo utilizzato per mandare un'email di verifica all'utente alla creazione dell'account
    private void sendEmailVerification() {

        final FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            user.sendEmailVerification()
                    .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(SignInActivity.this, "Email di verifica spedita a: " + user.getEmail(), Toast.LENGTH_LONG).show();
                            } else {
                                Log.e(TAG, "sendEmailVerification", task.getException());
                                Toast.makeText(SignInActivity.this, "Errore nell'invio della mail di verifica", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        }
    }


    //Crea un nuovo utente e lo inseriesce nel db
    private void writeNewUser(String userId, String name, String email){
        User user = new User(name, email,0,0, false);
        mDatabase.child("users").child(userId).setValue(user);
    }

    //Vengono gestite le varie opzioni al click dei vari bottoni
    @Override
    public void onClick(View v) {
        int i = v.getId();
        switch (i) {
            case R.id.button_sign_in:
                if (checkConnession()) {
                    signIn();
                } else {
                    alertDialogConnession();
                }
                break;
            case R.id.button_new_account:
                if (checkConnession()) {
                    newAccount();
                } else {
                    alertDialogConnession();
                }
                break;
            case R.id.button_reset_pwd:
                startActivity(new Intent(SignInActivity.this, ResetPwd.class));
                finish();
                break;
        }

    }

    //AlertDialog che avvisa che non c'è la connessione dati
    private void alertDialogConnession() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        builder.setTitle("CONNESSIONE DATI");
        builder.setMessage("Nessuna connessione dati, per poter creare un account o fare il login utilizza una connessione dati mobile oppure WI-FI");

        AlertDialog alert = builder.create();
        alert.show();
    }

    //AlertDialog che avvisa al primo avvio dell'app che servono i permessi
    private void alertDialogPerm() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        builder.setTitle("PERMESSI RICHIESTI!!!");
        builder.setMessage("L'app richiede i permessi di lettura e scrittura in memoria."+"\n"+"Negando tale richiesta l'app non funzionerà!");
        builder.setCancelable(false);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                if (Build.VERSION.SDK_INT>=23) {
                    ActivityCompat.requestPermissions(SignInActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
                }
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    //Metodo che legge il valore del numero di tentativi dell'utente tramite dataSnapshot e incrementa di 1 tale valore
    private void tentRestart() {
        if(mAuth.getCurrentUser() != null) {
            mDatabase.child("users").child(mAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(
                    new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            User user1 = dataSnapshot.getValue(User.class);
                            if (user1 != null) {
                                int numTentNow = user1.getNumTent();
                                numTentNow++;
                                mDatabase.child("users").child(mAuth.getCurrentUser().getUid()).child("numTent").setValue(numTentNow);
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    //Override del metodo onBackPressed() per gestire cosa deve venire fatto premendo il tasto back
    @Override
    public void onBackPressed(){
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


}
