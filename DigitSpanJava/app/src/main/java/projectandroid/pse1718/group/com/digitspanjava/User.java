package projectandroid.pse1718.group.com.digitspanjava;

import com.google.firebase.database.IgnoreExtraProperties;

@SuppressWarnings({"WeakerAccess", "unused"})
@IgnoreExtraProperties
//Classe che definisce l'utente del database
public class User {
    public String username;
    public String email;
    public int numTent;
    public int score;
    public boolean in_game;

    //Costruttore di default per poter essere richiamato da DataSnapshot.getValue(User.class)
    public User() { }

    //Costruttore con parametri (username, email, score, numeroTentativi, utente in gioco)
    public User(String username, String email, int score, int numTent, boolean in_game) {
        this.username = username;
        this.email = email;
        this.score = score;
        this.numTent = numTent;
        this.in_game = in_game;

    }

    //Metodi get per ritornare parametri (tentativi, username, score)
    public int getNumTent(){ return this.numTent; }

    public String getUsername() {
        return this.username;
    }

    public int getScore() {
        return this.score;
    }

}
