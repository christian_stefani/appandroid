package projectandroid.pse1718.group.com.digitspanjava;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

//Classe che visualizza il DialogFragment nella GameActivity (quando si è in ascolto della sequenza)
public class MyDialogFragment extends DialogFragment {

    public MyDialogFragment() {
    }

    @Override
    public void onDestroyView() {
        Dialog dialog = getDialog();
        if (dialog != null && getRetainInstance()) {
            dialog.setDismissMessage(null);
        }
        super.onDestroyView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    //Viene creata la rispettiva View tramite il file di layout specificato, settandola non cancellabile
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View MainView = inflater.inflate(R.layout.fragment_dialog, container, false);
        getDialog().setTitle("Text to Speech");
        setCancelable(false);
        return MainView;
    }

}
