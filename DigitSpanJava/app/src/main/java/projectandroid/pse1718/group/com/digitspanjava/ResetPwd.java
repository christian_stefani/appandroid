package projectandroid.pse1718.group.com.digitspanjava;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

//Classe che definisce l'activity di reset della password di un utente
public class ResetPwd extends AppCompatActivity {

    private EditText edtEmail;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_pwd);

        edtEmail = findViewById(R.id.edt_reset_email);
        Button btnResetPassword = findViewById(R.id.btn_reset_password);
        Button btnBack = findViewById(R.id.btn_back);

        mAuth = FirebaseAuth.getInstance();

        /*Quando viene cliccato il bottone Reset Password
        Se è presente la connessione dati viene inviata una mail per poter resettare la password all'utente specificato
        Altrimenti visualizza il messaggio che non è presente la connessione
         */
        btnResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkConnession()) {
                    String email = edtEmail.getText().toString().trim();

                    if (TextUtils.isEmpty(email)) {
                        Toast.makeText(getApplicationContext(), "Inserisci la tua email!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    mAuth.sendPasswordResetEmail(email)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(ResetPwd.this, "Controlla la mail per resettare la password!", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(ResetPwd.this, "Errore nell'invio della mail!", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                }
                else {
                    alertDialogConnession();
                }
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ResetPwd.this, SignInActivity.class));
                finish();
            }
        });
    }

    //Metodo che restituisce true se è presente le connessione dati, false altrimenti
    private boolean checkConnession() {
        ConnectivityManager manager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (manager != null) {
            NetworkInfo activeNetwork = manager.getActiveNetworkInfo();
            return activeNetwork != null && (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE);
        }
        return false;
    }

    //AlertDialog che viene visualizzato nel caso non sia presente la connessione
    private void alertDialogConnession() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        builder.setTitle("CONNESSIONE DATI");
        builder.setMessage("Nessuna connessione dati, per poter resettare la password utilizza una connessione dati mobile oppure WI-FI");

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onBackPressed(){
        startActivity(new Intent(ResetPwd.this, SignInActivity.class));
        finish();
    }

}
