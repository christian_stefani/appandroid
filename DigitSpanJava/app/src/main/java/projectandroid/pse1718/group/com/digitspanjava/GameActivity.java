package projectandroid.pse1718.group.com.digitspanjava;

import android.app.FragmentManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;
import java.util.Random;

//Classe che definisce l'activity di gioco
public class GameActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final String STRING_INSERT = "text";
    private static final String STRING_NUMBER = "text_num";
    private static final String STRING_RETRY = "retry_count";
    private static final String STRING_COUNT_BUTTON = "count_button";
    public static final String STRING_INTENT_SEQ = "sequen";
    public static final String STRING_INTENT_BUTTON = "countButton";


    private String sx;                              // sequenza da ascoltare
    public static int count=5;                      // numeri da cui e' composta la sequenza
    private int[] numSeq = new int[count];
    private int retry=0;
    private int countButton = 0;
    private Intent serviceTTS;
    private boolean mBounded;
    private final DatabaseReference mDatabase = SignInActivity.getDatabase().getReference();
    private final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            onRestoreInstanceState(savedInstanceState);
        }

        this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        setContentView(R.layout.activity_game);

        //Vengono visualizzate le istruzioni la prima volta che viene avviato il gioco
        if (isFirstRun()) {
            alertDialogFirstTime();         //regole del gioco spiegate all'utente
        }

        Button startB = findViewById(R.id.startButton);
        serviceTTS = new Intent(getApplicationContext(),TTS.class);

        //Viene calcolata una sequenza casuale di numeri interi ed inserita in una stringa
        numSeq = sequen(count);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < count; i++) {
            sb.append(numSeq[i]);
        }
        sx = sb.toString();

        final TextView counter = findViewById(R.id.random_number_text_view);
        String texCounter = "Sequenza: "+count+" caratteri";
        counter.setText(texCounter);

        final EditText mnum = findViewById(R.id.sequence);
        final FragmentManager fm = getFragmentManager();
        final MyDialogFragment dialogFragment = new MyDialogFragment();

        bindService(serviceTTS, mConnection, BIND_AUTO_CREATE);

        /*Bottone per avviare l'ascolto della sequenza
        Viene richiamato il servizio del Text to speech
        viene sospesa l'activity e richiamata dopo un tempo calcolato in base alla lunghezza della sequenza
        il campo per poter inserire la sequenza viene nascosto durante questo intervallo, viene visualizzato alla fine
         */
        startB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countButton++;
                if(countButton<=2) {
                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    mnum.setVisibility(View.GONE);
                    mnum.getText().clear();                         //cancella il contenuto dell'EditText
                    dialogFragment.show(fm, "Dialog Fragment");
                    serviceTTS.putExtra(STRING_INTENT_SEQ,numSeq);
                    serviceTTS.putExtra(STRING_INTENT_BUTTON,countButton);
                    startService(serviceTTS);
                    final Runnable r = new Runnable() {
                        public void run() {
                            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                            dialogFragment.dismissAllowingStateLoss();
                            mnum.setVisibility(View.VISIBLE);
                        }
                    };
                    final Handler handler = new Handler();
                    handler.postDelayed(r, count*2200);
                }
            }

        });

        //Metodo utilizzato per gestire cosa deve venir fatto nell'EditText in base al tasto cliccato della tastiera
        num_onClick(mnum);

        //Inizializzazione toolbar, DrawerLayout e NavigationView
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView =  navigationView.getHeaderView(0);
        TextView navUsername = headerView.findViewById(R.id.textViewEmail);
        navUsername.setText(Objects.requireNonNull(firebaseAuth.getCurrentUser()).getEmail());
    }

    //Gestione delle azioni quando viene cliccato il tasto back
    @Override
    public void onBackPressed() {
            System.gc();
            System.exit(0);
    }

    /*Gestione delle 3 opzioni del NavigationDrawer
    -logout dell'utente
    -cancellazione account
    -visualizzazione classifica
     */
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        int id = item.getItemId();
        switch (id) {
            case R.id.action_logout:
                drawer.closeDrawer(GravityCompat.START);
                if (checkConnession()) {
                    alertDialogLogout();
                } else {
                    alertDialogConnession();
                }
                return true;
            case R.id.action_delete:
                drawer.closeDrawer(GravityCompat.START);
                if (checkConnession()) {
                    alertDialogDelete();
                } else {
                    alertDialogConnession();
                }
                return true;
            case R.id.action_score:
                drawer.closeDrawer(GravityCompat.START);
                alertDialogScore();
                return true;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //Metodo utilizzato per vedere se è la prima volta che viene avviata l'activity di gioco
    private boolean isFirstRun() {
        boolean runBefore = MySharedPreferences.getSharedPreferencesBoolean(getApplicationContext(), MySharedPreferences.SharedPreferencesKeys.runFirst, false);
        if (!runBefore) {
            MySharedPreferences.putSharedPreferencesBoolean(getApplicationContext(),MySharedPreferences.SharedPreferencesKeys.runFirst, true);
        }
        return !runBefore;
    }

    private int randomInt() {
        Random r = new Random();
        return r.nextInt(10);
    }

    private int increment() {
        count++;
        return count;
    }

    private int[] sequen(int count)
    {
        int i;
        int[] seq = new int[count];

        for (i = 0; i < count; i++) {
            seq[i] = randomInt();
        }
        return seq;
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString(STRING_INSERT,sx);
        savedInstanceState.putIntArray(STRING_NUMBER,numSeq);
        savedInstanceState.putInt(STRING_RETRY,retry);
        savedInstanceState.putInt(STRING_COUNT_BUTTON,countButton);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        sx = savedInstanceState.getString(STRING_INSERT, "0");
        numSeq = savedInstanceState.getIntArray(STRING_NUMBER);
        retry = savedInstanceState.getInt(STRING_RETRY);
        countButton = savedInstanceState.getInt(STRING_COUNT_BUTTON);
    }

    //Viene mandato un intent per far ripartire l'activity
    private void sendMessage() {
        Intent intent = new Intent(this, GameActivity.class);
        startActivity(intent);
        finish();
    }

    //Metodo quando vengono commessi 2 errori per andare nell'activity dei punteggi (ScoreRanking)
    private void sendRestart() {
        Intent intent = new Intent(this, ScoreRanking.class);
        count=5;
        startActivity(intent);
        finish();
    }

    /*Metodo per gestire cosa deve venir fatto nell'EditText in base al tasto cliccato della tastiera
    Se la risposta è corretta scrive il punteggio nel db, lo incrementa di 1 e riparte l'activity
    Se è sbagliata cancella la sequenza appena digitata, al secondo errore termina il gioco e viene visualizzata la classifica
     */
    private void num_onClick(EditText mnum) {
        mnum.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int KeyCode, KeyEvent event) {
                boolean handled = false;
                if ((KeyCode== EditorInfo.IME_ACTION_DONE) || (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))) {
                    String inputText = v.getText().toString();
                    if(inputText.equals(sx)) {
                        Toast.makeText(getApplicationContext(), R.string.correct_question, Toast.LENGTH_SHORT).show();
                        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        if (inputManager != null && getCurrentFocus() != null) {
                            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                                    InputMethodManager.HIDE_NOT_ALWAYS);
                        }
                        queryScore();
                        count=increment();
                        sendMessage();
                    }
                    else {
                        Toast.makeText(getApplicationContext(), R.string.incorrect_question, Toast.LENGTH_SHORT).show();
                        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        if (inputManager != null && getCurrentFocus() != null) {
                            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                                    InputMethodManager.HIDE_NOT_ALWAYS);
                        }
                        v.setText("");
                        retry=setTry();
                        if (Retry()) {
                            sendRestart();
                        }
                    }
                    handled = true;
                }
                return handled;
            }

        });
    }

    /*Metodo utilizzato per leggere il valore nel db del punteggio, ed eventualmente aggiornarlo
    se quello registrato localmente è più alto di quello del db online lo aggiorna
    altrimenti non fa nulla
     */
    private void queryScore() {
        final int localScore = count;
        if (firebaseAuth.getCurrentUser() != null) {
            Query query = mDatabase.child("users").child(firebaseAuth.getCurrentUser().getUid()).child("score");
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        Integer score = dataSnapshot.getValue(Integer.class);
                        if (score != null) {
                            if (localScore > score) {
                                mDatabase.child("users").child(firebaseAuth.getCurrentUser().getUid()).child("score").setValue(localScore);
                            }
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    //Metodo che ritorna l'UID (Identificatore di ogni utente in firebase)
    private String removeDataUser() {
        if (firebaseAuth.getCurrentUser() != null) {
            return firebaseAuth.getCurrentUser().getUid();
        }
        return null;
    }

    //Metodo che cancella un utente e i relativi dati nel db
    private void deleteAccount() {
        final String uid = removeDataUser();
        final FirebaseUser currentUser = firebaseAuth.getCurrentUser();
        if ((currentUser != null) && (uid != null)) {
            currentUser.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        mDatabase.child("users").child(uid).setValue(null);
                        startActivity(new Intent(GameActivity.this,SignInActivity.class));
                        finish();
                    } else {
                        alertDialogDeleteUser();
                    }
                }
            });
        }
    }

    /*Viene gestito il click del bottone back nel caso sia aperto il NavigationDrawer oppure no
    Se è aperto lo chiude
    Se non è aperto visualizza il messaggio contenuto nel alertDialogBack()
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
               alertDialogBack();
            }
            return true;
        }
        return false;
    }

    private boolean Retry() {
        return retry == 2;
    }

    private int setTry() {
        retry++;
        return retry;
    }

    //Oggetto di tipo ServiceConnection che gestisce la connessione e disconnessione con il service
    private final ServiceConnection mConnection = new ServiceConnection() {

        public void onServiceDisconnected(ComponentName name) {
            mBounded = false;
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            mBounded = true;
        }
    };

    //AlertDialog che avvisa quando viene premuta l'opzione di logout nel NavigationDrawer
    private void alertDialogLogout() {
        if (firebaseAuth.getCurrentUser() != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
            builder.setTitle("LOGOUT UTENTE");
            builder.setMessage("Se effettui il logout perderai l'attuale sessione di gioco, sei sicuro di voler proseguire?");
            builder.setCancelable(false);

            builder.setPositiveButton("SI", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    mDatabase.child("users").child(firebaseAuth.getCurrentUser().getUid()).child("in_game").setValue(false);
                    firebaseAuth.signOut();
                    count = 5;
                    startActivity(new Intent(GameActivity.this,SignInActivity.class));
                    finish();
                    dialog.dismiss();
                }
            });

            builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    //AlertDialog che avvisa quando viene premuta l'opzione di cancellazione account nel NavigationDrawer
    private void alertDialogDelete() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        builder.setTitle("CANCELLAZIONE UTENTE E DATI");
        builder.setMessage("Se prosegui cancellerai l'utente e i relativi dati, sei sicuro di voler proseguire?");
        builder.setCancelable(false);

        builder.setPositiveButton("SI", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                count=5;
                deleteAccount();
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    //AlertDialog che avvisa quando viene premuta l'opzione per visualizzare la classifica nel NavigationDrawer
    private void alertDialogScore() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        builder.setTitle("CLASSIFICA");
        builder.setMessage("Se prosegui per visualizzare la classifica perderai l'attuale sessione di gioco, sei sicuro di voler proseguire?");
        builder.setCancelable(false);

        builder.setPositiveButton("SI", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                count=5;
                startActivity(new Intent(GameActivity.this, ScoreRanking.class));
                finish();
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    /*AlertDialog che avvisa l'utente che non può cancellare l'account in quanto è scaduto il token generato da firebase per l'utente
    Viene avvisato che se vuole cancellare l'account deve prima effettuare il logout
    poi riautenticarsi e procedere alla cancellazione
     */
    private void alertDialogDeleteUser() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        builder.setTitle("CANCELLAZIONE UTENTE E DATI");
        builder.setMessage("E' tracorso troppo tempo dall'ultima verifica dell'account, se vuoi continuare dovrai riautenticarti e solo dopo potrai procedere alla cancellazione, sei sicuro di voler continuare?");
        builder.setCancelable(false);

        builder.setPositiveButton("SI", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                mDatabase.child("users").child(Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid()).child("in_game").setValue(false);
                firebaseAuth.signOut();
                startActivity(new Intent(GameActivity.this, SignInActivity.class));
                finish();
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    //AlertDialog quando viene premuto il tasto back
    private void alertDialogBack() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        builder.setTitle("ATTENZIONE!!!");
        builder.setMessage("Hai premouto il tasto back, se proseguirai perderai l'attuale sessione di gioco, sei sicuro di voler proseguire?");
        builder.setCancelable(false);

        builder.setPositiveButton("SI", new DialogInterface.OnClickListener() {

            public void onClick(final DialogInterface dialog, int which) {
                if(checkConnession()) {
                    mDatabase.child("users").child(Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid()).child("in_game").setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                onBackPressed();
                                dialog.dismiss();
                            } else {
                                Toast.makeText(GameActivity.this, "Errore", Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }
                        }
                    });
                }
                else {
                    onBackPressed();
                    dialog.dismiss();
                }
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    //AlertDialog che viene visualizzato solamente la prima volta che viene avviato il gioco
    private void alertDialogFirstTime() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        builder.setTitle("REGOLE");
        builder.setMessage("Ascolta la sequenza di numeri e scrivila nell'apposito campo. Se rispondi correttamente verrà aggiunto un numero alla sequenza successiva. Hai 2 tentativi per ogni livello");
        builder.setCancelable(false);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    //Metodo che restituisce true se è presente le connessione dati, false altrimenti
    private boolean checkConnession() {
        ConnectivityManager manager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (manager != null) {
            NetworkInfo activeNetwork = manager.getActiveNetworkInfo();
            return activeNetwork != null && (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE);
        }
        return false;
    }

    //AlertDialog per avvisare che non è possibile procedere con l'operazione in quanto non è presente nessuna connessione dati
    private void alertDialogConnession() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        builder.setTitle("CONNESSIONE DATI");
        builder.setMessage("Non è possibile procedere, non è presente nessuna connessione dati");

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        //Viene fatto il bind del service
        if(!mBounded) {
            bindService(serviceTTS, mConnection, BIND_AUTO_CREATE);
            mBounded = true;
        }
        //Viene settato nel database il valore true del campo in_game (ovvero l'utente è in gioco)
        if (firebaseAuth.getCurrentUser() != null) {
            mDatabase.child("users").child(Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid()).child("in_game").setValue(true);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //Viene fatto l'unbind del service
        if(mBounded) {
            unbindService(mConnection);
            mBounded = false;
        }
        //Viene settato nel database il valore false del campo in_game (ovvero l'utente non è attualmente in gioco)
        if (firebaseAuth.getCurrentUser() != null) {
            mDatabase.child("users").child(Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid()).child("in_game").setValue(false);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if(mBounded) {
            unbindService(mConnection);
            mBounded = false;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


}
