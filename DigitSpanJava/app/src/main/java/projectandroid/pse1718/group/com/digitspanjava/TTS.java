package projectandroid.pse1718.group.com.digitspanjava;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.speech.tts.TextToSpeech;

import java.util.Locale;

//Classe che definisce il servizio del Text to Speech
public class TTS extends Service implements TextToSpeech.OnInitListener{
    private TextToSpeech mTts;
    private int[] seq = new int[GameActivity.count];
    private int countButton;

    @Override
    public void onCreate() {
        super.onCreate();
        mTts = new TextToSpeech(this,this);
    }

    @Override
    public int onStartCommand (Intent intent, int flags, int startId) {
            mTts = new TextToSpeech(this,this);
            //Passaggio dei parametri tramite Intent dalla GameActivity al servizio TTS
            seq = intent.getIntArrayExtra(GameActivity.STRING_INTENT_SEQ);
            countButton = intent.getIntExtra(GameActivity.STRING_INTENT_BUTTON,0);
        return START_NOT_STICKY;
    }

    @Override
    public void onInit(int status) {

        /*
        Settaggio dell'oggetto mTts tramite relativi metodi
        setLanguage: lingua utilizzata dal tts
        setPitch: intonazione, altezza voce
        setSpeechRate: velocità di pronuncia
         */
        if (status != TextToSpeech.ERROR) {
            mTts.setLanguage(Locale.ITALY);
            mTts.setPitch(1f);
            mTts.setSpeechRate(1f);
        }
        if (mTts != null && countButton != 0 ) {
            for (int i = 0; i < GameActivity.count; i++) {
                String toSpeach = Integer.toString(seq[i]);
                //metodo speak che legge la stringa che gli viene passata aggiungendo le nuove entry in fondo alla coda
                mTts.speak(toSpeach, TextToSpeech.QUEUE_ADD, null, null);
                //metodo per intervallare con una pausa da un numero all'altro di 1.3 secondi
                mTts.playSilentUtterance(1300, TextToSpeech.QUEUE_ADD, null);
            }
        }

    }

    @Override
    public void onDestroy() {
       //Stop del servizio
       if (mTts != null) {
            mTts.stop();
            mTts.shutdown();
        }
        super.onDestroy();
    }

    //Metodo per fare bind del service, che in questo caso non viene fatto
    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

}
